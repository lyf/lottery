## 声明
源码只供学习使用，如用于商业活动与本人无关，请勿将系统用于非法业务

## 截图

| 投注 | 投注订单详情 | 追号 |
| :------: | :------: | :------: |
| ![投注](https://images.gitee.com/uploads/images/2019/0527/133649_487ecd0c_527945.png) | ![投注订单详情](https://images.gitee.com/uploads/images/2019/0527/133649_9777ff81_527945.png) | ![追号](https://images.gitee.com/uploads/images/2019/0527/133650_46a7b15d_527945.png) |

| 投注记录 | 充值 | 提现 |
| :------: | :------: | :------: |
| ![投注记录](https://images.gitee.com/uploads/images/2019/0527/133649_e6fd6ea8_527945.png) | ![充值](https://images.gitee.com/uploads/images/2019/0527/133649_127812be_527945.png) | ![提现](https://images.gitee.com/uploads/images/2019/0527/133649_e62f1b03_527945.png) |

> 由于程序不断优化，界面细节可能有所变化，请以实际页面为准

> 作者不喝咖啡 :coffee: 只喝茶 :tea: 觉得有帮助的可以请我喝茶


| 支付宝 | 微信支付 |
| :------: | :------: |
| ![alipay](https://images.gitee.com/uploads/images/2019/0527/133651_9a771621_527945.jpeg) | ![wepay](https://images.gitee.com/uploads/images/2019/0527/133652_b088b2a9_527945.png) |

