package me.zohar.lottery.constants;

public class Constant {

	public static final String 投注订单状态_未开奖 = "1";

	public static final String 投注订单状态_未中奖 = "2";

	public static final String 投注订单状态_已中奖 = "3";

	public static final String 投注订单状态_已撤单 = "4";

	public static final String 追号订单状态_进行中 = "1";

	public static final String 追号订单状态_已完成 = "2";

	public static final String 追号订单状态_已取消 = "3";

	public static final String 账号类型_管理员 = "admin";

	public static final String 账号类型_代理 = "agent";

	public static final String 账号类型_会员 = "member";

	public static final String 账号状态_启用 = "1";

	public static final String 账号状态_禁用 = "2";

	public static final String 游戏状态_启用 = "1";

	public static final String 游戏状态_禁用 = "2";

	public static final String 游戏状态_维护中 = "3";

	public static final String 期号状态_未开奖 = "1";

	public static final String 期号状态_已开奖 = "2";

	public static final String 期号状态_已结算 = "3";

	public static final String 期号状态_已作废 = "4";

	public static final String 游戏当期状态 = "_GAME_CURRENT_ISSUE_STATE";

	public static final String 游戏当期状态_可以投注 = "1";

	public static final String 游戏当期状态_已截止投注 = "2";

	public static final String 游戏当期状态_休市中 = "3";

	public static final String 游戏玩法状态_启用 = "1";

	public static final String 游戏玩法状态_禁用 = "0";

	public static final String 游戏当前期号 = "_GAME_CURRENT_ISSUE";

	public static final String 当前开奖期号ID = "CURRENT_LOTTERY_ISSUE_ID";

	public static final Integer 充值订单默认有效时长 = 10;

	public static final String 充值订单_已支付订单单号 = "RECHARGE_ORDER_PAID_ORDER_NO";

	public static final String 充值订单状态_待支付 = "1";

	public static final String 充值订单状态_已支付 = "2";

	public static final String 充值订单状态_已结算 = "3";

	public static final String 充值订单状态_超时取消 = "4";

	public static final String 充值订单状态_人工取消 = "5";

	public static final String 账变日志类型_账号充值 = "1";

	public static final String 账变日志类型_充值优惠 = "2";

	public static final String 账变日志类型_投注扣款 = "3";

	public static final String 账变日志类型_投注返奖 = "4";

	public static final String 账变日志类型_账号提现 = "5";

	public static final String 账变日志类型_活动礼金 = "6";

	public static final String 账变日志类型_撤单返款 = "7";

	public static final String 充提日志订单类型_充值 = "1";

	public static final String 充提日志订单类型_提现 = "2";

	public static final String 提现记录状态_发起提现 = "1";

	public static final String 提现记录状态_审核通过 = "2";

	public static final String 提现记录状态_审核不通过 = "3";

	public static final String 提现记录状态_已到账 = "4";

	public static final String 赔率模式_固定赔率 = "1";

	public static final String 赔率模式_不固定赔率 = "2";

	public static final String 游戏_广东11选5 = "GD11X5";

	public static final String 游戏_江西11选5 = "JX11X5";

	public static final String 游戏_江苏11选5 = "JS11X5";

	public static final String 游戏_上海11选5 = "SH11X5";

	public static final String 游戏_浙江11选5 = "ZJ11X5";

	public static final String 游戏_重庆时时彩 = "CQSSC";

	public static final String 游戏_新疆时时彩 = "XJSSC";

	public static final String 游戏_云南时时彩 = "YNSSC";

	public static final String 游戏_天津时时彩 = "TJSSC";

}
